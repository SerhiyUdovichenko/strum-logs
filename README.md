# Strum::Logs

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/strum/logs`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'strum-logs'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install strum-logs

## Usage

In order to start working with this gem, following configuration must be enabled:

```
StrumLogs::Configuration.configure do |config|
  config.application_version = "1.0.0"
  config.application_name = "app name"
  config.redis_instrumentation = true
  config.rack_instrumentation = true
  config.environment = "development"
end
```

This configuration allows to use rack and redis open telemetry tracing.
Redis logger can be enabled using following configuration:
```
StrumLogs::Configuration.configure do |config|
  config.redis_after_call_hooks = [StrumLogs::Redis::Logger.new]
end
```
Example of redis logs:
```
{
             :request => "get hello",
             :message => "redis request",
    :response_message => "world",
        :service_name => "app name",
             :version => "1.0.0",
            :trace_id => "0973f2d0c2f92ce3b29e41638d09bc89",
             :span_id => "37fc1030e7b8413c"
}
```
Rack logging can be enabled as rack middleware:

```
require 'roda'
require 'strum_logs'

class AppName < Roda
  use StrumLogs::Rack::RequestLogMiddleware
```

Example of rack logs:
```
{
              :method => "GET",
                :path => "/wp-debts/info",
               :query => "",
            :protocol => "HTTP/1.1",
          :started_at => 2022-08-02 11:04:15.588912879 +0300,
                :peer => "127.0.0.1",
             :message => "HTTP Request",
                :user => nil,
              :status => 200,
             :headers => {
          "Content-Type" => "application/vnd.api+json",
        "Content-Length" => "20",
           "traceparent" => "00-0973f2d0c2f92ce3b29e41638d09bc89-37fc1030e7b8413c-01"
    },
    :response_message => "{\"data\":[],\"meta\":0}",
          :log_status => "success",
             :request => nil,
            :finished => 2022-08-02 11:04:17.549062761 +0300,
          :elapsed_ms => 1960.1499,
        :service_name => "app name",
             :version => "1.0.0",
            :trace_id => "0973f2d0c2f92ce3b29e41638d09bc89",
             :span_id => "37fc1030e7b8413c"
}
```
Faraday logs can be enabled by using Faraday middleware:
```
require 'strum_logs'
conn = Faraday.new(url: https://example.com/, headers: {}) do |c|
        c.use StrumLogs::Faraday::RequestLogMiddleware
      end
```
To enable faraday open telemetry tracing, following config should be enabled:
```
StrumLogs::Configuration.configure do |config|
  config.faraday_instrumentation = true
end
```
To enable strum esb logs, following configuration must be added to strum esb configs:
```
Strum::Esb.configure do |config|
  config.before_publish_hooks = [StrumLogs::StrumEsbHooks.method(:before_publish_hook)]
  config.after_handler_hooks = [StrumLogs::StrumEsbHooks.method(:after_handler_hook)]
end
```
Strum esb logs example:
```
{
         :message => "Handling AMQP message",
            :body => "{ \"hello\": \"world\" }",
      :properties => {
        :consumer_tag => "bunny-1659424153000-768018451868",
        :delivery_tag => #<Bunny::VersionedDeliveryTag:0x0000558005b4edf0 @tag=1, @version=0>,
         :redelivered => false,
            :exchange => "",
         :routing_key => "test_hooks",
            :consumer => #<Bunny::Consumer:9180 @channel_id=1 @queue=test_hooks> @consumer_tag=bunny-1659424153000-768018451868 @exclusive= @no_ack=false>,
             :channel => #<Bunny::Channel:9200 @id=1 @connection=#<Bunny::Session:0x4628 guest@rabbit.lvh.me:5672, vhost=/, addresses=[rabbit.lvh.me:5672]>> @open=true
    },
        :protocol => "AMQP",
        :metadata => {
              :headers => {
              "action" => "test",
            "resource" => "hello"
        },
        :delivery_mode => 1
    },
    :service_name => "working permits debts",
         :version => "1.0.1",
        :trace_id => "00000000000000000000000000000000",
         :span_id => "0000000000000000"
}

```
To use rabbit open telemetry traces, following configuration should be set:
```
StrumLogs::Configuration.configure do |config|
  config.rabbit_instrumentation = true
end
```
To use sequel logger, strum logger should be added to sequel loggers:
```
DB.logger = StrumLogs::Sequel::Logger.new
```
To user httparty logger, hooks should be enabled:
```
StrumLogs::Configuration.configure do |config|
  config.httparty_after_call_hooks = [StrumLogs::Httparty::Logger.new]
end
```
## Configuration options
Gem has a number of configuration options. Note, that some of them are required to be specified in order for gem to be working

```
StrumLogs::Configuration.configure do |config|
  config.stdout_sync = true                  # Sets std_out_sync to true, true by default
  config.level = Logger::INFO                # Sets log level, info by default
  config.application_version = "1.0.0"       # Sets app version, mandatory configuration
  config.application_name = "app name"       # Sets app name, mandatory field
  config.redis_instrumentation = false       # Turns on redis logger and open telemetry tracing for redis, false by default
  config.rack_instrumentation = false        # Turns on rack open telemetry traces, false by default
  config.faraday_instrumentation = true      # Turns on faraday open telemetry traces, false by default
  config.rabbit_instrumentation = true       # Turns on bunny open telemetry traces, false by default
  config.stack_trace = true                  # Turns on error stack trace field, true by default
  config.environment = ENV["RACK_ENV"]       # Is set by RACK_ENV environmental variable, development by default
  config.enable_export_spans = false         # Span export, false by default
  config.redis_after_call_hooks = []         # Is used to add hooks after redis call (currently is used to add log hook)
  config.team_name = "team name"             # Is used to set team name, that is responsible for service
end
```
## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/strum-logs. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/strum-logs/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Strum::Logs project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/strum-logs/blob/master/CODE_OF_CONDUCT.md).
