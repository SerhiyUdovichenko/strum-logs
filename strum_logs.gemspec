# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "strum_logs/version"

Gem::Specification.new do |spec|
  spec.name          = "strum-logs"
  spec.version       = ::StrumLogs::VERSION
  spec.authors       = ["Serhiy Nazarov"]
  spec.email         = ["sn@nazarov.com.ua"]

  spec.summary       = "Logs"
  spec.description   = "Write a longer description or delete this line."
  spec.require_paths = ["lib"]
  spec.homepage      = "https://gitlab.com/strum-rb/strum-logs"
  spec.license       = "MIT"

  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")
  spec.metadata = {
    "rubygems_mfa_required" => "true"
  }
  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }

  spec.add_runtime_dependency "opentelemetry-common", "~> 0.19"
  spec.add_runtime_dependency "opentelemetry-exporter-otlp", "~> 0.22"
  spec.add_runtime_dependency "opentelemetry-instrumentation-bunny", "~> 0.19"
  spec.add_runtime_dependency "opentelemetry-instrumentation-faraday", "~> 0.21"
  spec.add_runtime_dependency "opentelemetry-instrumentation-pg", "~> 0.21"
  spec.add_runtime_dependency "opentelemetry-instrumentation-rack", "~> 0.21"
  spec.add_runtime_dependency "opentelemetry-instrumentation-redis", "~> 0.23"
  spec.add_runtime_dependency "opentelemetry-instrumentation-sinatra", "~> 0.20"
  spec.add_runtime_dependency "opentelemetry-sdk", "~> 1.1"
  spec.add_runtime_dependency "sequel", "~> 5.24"

  spec.add_runtime_dependency "amazing_print", "~> 1.4"
  spec.add_runtime_dependency "ougai", "~> 2.0"
  spec.add_development_dependency "faraday", "~> 1.10.0"
  spec.add_development_dependency "faraday_middleware", "~> 1.2.0"
  spec.add_runtime_dependency "rack", "~> 2.2.4"
  spec.add_development_dependency "rake", "~> 13"
  spec.add_runtime_dependency "httparty"
  spec.add_development_dependency "bunny-mock"
  spec.add_development_dependency "webmock", "< 2.0.0"
  spec.add_runtime_dependency "redis"
  spec.add_development_dependency "rubocop", "~> 1.30"
  spec.add_development_dependency "strum-esb"
  spec.add_runtime_dependency "dry-container", "< 0.10.1"
  spec.add_runtime_dependency "dry-configurable", "~> 0.12.1"
  spec.add_runtime_dependency "dry-types", "~> 1.4.0"
  spec.add_runtime_dependency "strum-pipe", "~> 0.0.3"
  spec.add_runtime_dependency "strum-service", "~> 0.2.1"
end
