# frozen_string_literal: true

require 'httparty'
require "bundler/setup"
require "strum_logs"
require "minitest/autorun"
require "minitest/spec"
require "rack"
require "oj"
require "faraday"
require 'redis'
require 'strum/esb'
require 'webmock/minitest'
require 'bunny-mock'

include WebMock::API
WebMock.enable!

ENV["RACK_ENV"] = "test"

Strum::Esb.configure do |config|
  config.before_publish_hooks = [StrumLogs::StrumEsbHooks.method(:before_publish_hook)]
  config.before_handler_hooks = [StrumLogs::StrumEsbHooks.method(:before_handler_hook)]
  config.after_handler_hooks = [StrumLogs::StrumEsbHooks.method(:after_handler_hook)]
  # config.rabbit_channel_pool = ConnectionPool.new(size: 1, timeout: 1) { BunnyMock.new.start.channel }
end

StrumLogs::Configuration.configure do |config|
  config.black_list_keys = ["password"]
  config.application_name = "test"
  config.application_version = "1.0.1"
  config.team_name = "test"
  config.redis_after_call_hooks = [StrumLogs::Redis::Logger.new]
  config.httparty_after_call_hooks = [StrumLogs::Httparty::Logger.new]
end

def logs_response(&block)
  response = nil
  out, _err = capture_subprocess_io do
    response = yield
  end
  puts out
  logs = Oj.load(out.match(/^\{.*}/).to_s)
  [response, logs]
end
