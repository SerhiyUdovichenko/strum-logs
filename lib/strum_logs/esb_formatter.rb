# frozen_string_literal: true

module StrumLogs
  class EsbFormatter
    def self.call(severity, time, _program_name, message)
      {
        level: severity,
        pid: Process.pid,
        message: message[:msg],
        started_at: time.utc.iso8601
      }.merge!(message)
    end
  end
end
