# frozen_string_literal: true

module StrumLogs
  module Helpers
    module HttpLogHelper
      def error_process(log_entity, error, span)
        log_entity[:error] = error.message
        log_entity[:stack_trace] = error.backtrace if Configuration.config.stack_trace
        headers = { "content-type" => "application/json" }
        response = { trace_id: span.context.hex_trace_id, span_id: span.context.hex_span_id, error: error.message }
        span.record_exception(error)
        [500, headers, [Oj.dump(response)]]
      end

      def logg_process(log_entity, env)
        log_entity[:request] = env["roda.json_params"]
        log_entity[:elapsed_ms] = elapsed_ms(log_entity)
        output(log_entity)
      end

      def elapsed_ms(log_entity)
        log_entity[:finished] = Time.now
        ((log_entity[:finished] - log_entity[:started_at]) * 1000).round(4)
      end

      def output(log_entity)
        if log_entity[:error]
          @logger.error(log_entity)
        else
          @logger.info(log_entity)
        end
      end

      def parse_body(body)
        case body
        when Hash
          body
        when Array
          body.first
        else
          body
        end
      end
    end
  end
end
