# frozen_string_literal: true

module StrumLogs
  class LoggingSpanExporter
    def initialize
      @stopped = false
    end

    def export(spans, _timeout: nil)
      return FAILURE if @stopped

      Array(spans).each { |s| StrumLogs::DefaultLogger.logger.info(s) }

      SUCCESS
    end

    def force_flush(_timeout: nil)
      SUCCESS
    end

    def shutdown(_timeout: nil)
      @stopped = true
      SUCCESS
    end
  end
end
