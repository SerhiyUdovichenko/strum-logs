# frozen_string_literal: true

require "sequel"
require "strum_logs/default_logger"

module StrumLogs
  module Sequel
    class Logger
      def initialize
        @logger = StrumLogs::DefaultLogger.instance.logger
      end

      def public_send(level, message)
        logs = { message: "sequel request" }
        @logger.error(logs.merge({ error: message })) && return if level == :error

        @logger.info(logs.merge({ request: message }))
      end
    end
  end
end
