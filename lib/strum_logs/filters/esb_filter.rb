require "strum_logs/helpers/filters"

module StrumLogs
  module Filters
    class EsbFilter
      include StrumLogs::Helpers::Filters

      def filter!(log_entity)
        filter_body(log_entity)
        log_entity
      end

      private

      def filter_body(log_entity)
        parsed_body = parse_json(log_entity[:body])

        case parsed_body
        when Hash
          log_entity[:body] = filter_hash(parsed_body).to_json
        when Array
          log_entity[:body] = filter_array(parsed_body).to_json
        else
          log_entity
        end
      end
    end
  end
end
